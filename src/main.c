#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mem.h"

void debug(const char *fmt, ...);

void test_1() {
  debug("Test allocating 3 heaps\n");

  void *heap = heap_init(0);

  void *block_1 = _malloc(sizeof(uint64_t));
  void *block_2 = _malloc(sizeof(uint64_t));
  void *block_3 = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(block_1);
  _free(block_2);
  _free(block_3);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_2() {
  debug("Test freeing one\n");

  void *heap = heap_init(0);

  _malloc(sizeof(uint64_t));
  void *block = _malloc(sizeof(uint64_t));
  _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(block);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_3() {
  debug("Test freeing two\n");

  void *heap = heap_init(0);

  _malloc(sizeof(uint64_t));
  void *block_1 = _malloc(sizeof(uint64_t));
  void *block_2 = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(block_1);
  _free(block_2);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_4() {
  debug("Test memory expansion\n");

  void* heap = heap_init(0);

  while (_malloc(sizeof(uint64_t)) != NULL)
    ;
  
  debug_heap(stdout, heap);

  void* block = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(block);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_5() {
  debug("Test memory allocation in different region\n");

  void* heap = heap_init(0);

  while (_malloc(sizeof(uint64_t)) != NULL)
    ;

  debug_heap(stdout, heap);
  
  void* block = _malloc(sizeof(uint64_t));
  void* block_2 = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(block);
  _free(block_2);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

int main() {
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  return 0;
}